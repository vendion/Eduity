package eduity::Controller::Profile;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

eduity::Controller::Profile - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base : Chained('/'): PathPart('profile'):CaptureArgs(0) {
	my ( $self, $c ) = @_;

	$c->stash( users_rs    => $c->model('DB::User'));
	$c->stash( profiles_rs => $c->model('DB::Profile'));
	$c->stash( userskills_rs   => $c->model('DB::Userskill'));
	$c->stash( skills_rs => $c->model('DB::Skill'));
	$c->stash( contacts_rs => $c->model('DB::Contact'));
	$c->stash( future_rs => $c->model('DB::Futureskill'));
	$c->stash( challanges_rs => $c->model('DB::Challange'));
}
=head2 index

=cut

sub home : Chained('base'): Args(0) {
	my ( $self, $c ) = @_;

	if ( $c->user_exists() ) {
		my $user = $c->stash->{users_rs}->find({ id => $c->user->id });
		die "no such user\n" if (! $user);
		$c->stash(user => $user);
		my $profile = $c->stash->{profiles_rs}->find({ id => $c->user->id });
		die "no such profile\n" if (! $profile);
		$c->stash(profile => $profile);
		my $skills = $c->stash->{userskills_rs}->search(
			undef,
			{
				where => { 'userid' => $c->user->id },
				order_by => { -asc => 'skill' },
				rows  => 50,
				page  => 1,
			},
		);
		$c->stash(skills => $skills);
		my $future = $c->stash->{future_rs}->search(
			undef,
			{
				where => { 'userid' => $c->user->id },
				order_by => { -asc => 'skill' },
				rows => 50,
				page => 1,
			},
		);
		$c->stash(future => $future);
	}
	else {
		$c->stash->{'error_msg'} = 'You need to be logged in to view this page';
		$c->res->redirect( $c->uri_for('/') );
	}
}

sub edit : Chained('base'): Args(0) {
	my ( $self, $c ) = @_;

	if ( $c->user_exists() ) {
		my $skills = $c->stash->{skills_rs}->search(
			undef,
			{
				rows => 50,
				page => 1,
			},
		);
		$c->stash(skills => $skills);
		my $current = $c->stash->{userskills_rs}->search(
			undef,
			{
				where => { 'userid' => $c->user->id },
				rows  => 50,
				page  => 1,
			},
		);
		$c->stash(current => $current);
		my $goals = $c->stash->{future_rs}->search(
			undef,
			{
				where => { 'userid' => $c->user->id },
				rows  => 50,
				page  => 1,
			},
		);
		$c->stash(goals => $goals);
		my $profile = $c->stash->{profiles_rs}->find({ userid => $c->user->id });
		die "no such profile\n" if (! $profile);
		$c->stash(profile => $profile );
		if ( lc($c->req->method) eq 'post' ) {
			my $params = $c->req->params;
			if ( ! defined($params->{firstname}) ) {
				$c->stash( error_msg => 'Please give your first name');
				return;
			}
			if ( ! defined($params->{lastname}) ) {
				$c->stash( error_msg => 'Please give your last name');
				return;
			}
			my $users = $c->user;
			eval { $users->update({
						firstname => $params->{firstname},
						lastname  => $params->{lastname},
					})};
			my $profile = $c->stash->{profile};
			eval { $profile->update({
						bio => $params->{bio},
					})};
			if ( $params->{skill} ) {
				my $skills = $c->stash->{current};
				eval { $skills->create({
							skill => $params->{skill},
							userid => $c->user->id,
							rating => 1,
						})};
			}
			if ( $params->{goal} ) {
				my $goals = $c->stash->{goals};
				eval { $goals->create({
						skill => $params->{goal},
						userid => $c->user->id,
					});
				};
			}
		}
	}
	else {
		$c->stash->{'error_msg'} = 'You need logged in to view this page';
		$c->res->redirect( $c->uri_for('/') );
	}
}

sub search : Chained('base'): Args(0) {
	my ( $self, $c ) = @_;

	if ( lc($c->req->method) eq 'post' ) {
		my $params = $c->req->params;

		my $matches = $c->stash->{users_rs}->search(
			undef,
			{
				email => { like => '%' . $params->{email} . '%' },
				rows  => 50,
				page  => 1,
			},
		);
		$c->stash( matches => $matches );
	}
	else {
		$c->res->redirect( $c->uri_for('/') );
	}
}

sub view : Chained('base'): Args(1) {
	my ( $self, $c, $id ) = @_;

	my $user = $c->stash->{users_rs}->find({ id => $id });
	die "no such user\n" if (! $user);
	$c->stash(user => $user);
	my $profile = $c->stash->{profiles_rs}->find({ id => $id });
	die "no such profile\n" if (! $profile);
	$c->stash(profile => $profile);
	my $skills = $c->stash->{userskills_rs}->search(
		undef,
		{
			where => { 'userid' => $id },
			order_by => { -asc => 'skill' },
			rows  => 50,
			page  => 1,
		},
	);
	$c->stash(skills => $skills);
	my $future = $c->stash->{future_rs}->search(
		undef,
		{
			where => { 'userid' => $id },
			order_by => { -asc => 'skill' },
			rows => 50,
			page => 1,
		},
	);
	$c->stash(future => $future);
}

sub user : Chained('base'): Args(3) {
	my ( $self, $c, $id, $change, $skillid ) = @_;

	if ( $c->user_exists ) {
		my $skill = $c->stash->{userskills_rs}->find({ id => $skillid });
		die "no such skill\n" if (! $skill);
		$c->log->info('Current rating: ' . $skill->rating);
		my $rating = $skill->rating;
		if ( lc($change) eq 'minus' ) {
			if ( $rating > 1 ) {
				$rating--;
			}
		}
		else {
			if ( $rating < 10 ) {
				$rating++;
			}
		}
		eval { $skill->update({
					rating => $rating,
				});
		};
		return $c->res->redirect( $c->uri_for("/profile/view/$id") );
	}
	else {
		$c->stash->{'error_msg'} = 'You need to be logged in to do that';
		$c->res->redirect( $c->uri_for('/') );
	}
}

sub follow : Chained('base'): Args(1) {
	my ( $self, $c, $id ) = @_;

	if ( $c->user_exists() ) {
		my $contact = $c->stash->{contacts_rs};
		eval { $contact->create({
				userid => $c->user->id,
				contactid => $id,
				});
		};
	return $c->res->redirect( $c->uri_for("/profile/view/$id") );	
	}
	else {
		$c->stash->{'error_msg'} = 'You need to be logged in to do that';
		$c->res->redirect( $c->uri_for("/") );
	}
}

sub unfollow : Chained('base'): Args(1) {
	my ( $self, $c, $id ) = @_;

	if ( $c->user_exists() ) {
		my $contact = $c->stash->{contacts_rs}->find({ contactid => $id });
		$contact->delete();
		$c->res->redirect( $c->uri_for("/profile/view/$id") );
	}
	else {
		$c->stash->{'error_msg'} = 'You need to be logged in to do that';
		$c->res->redirect( $c->uri_for("/") );
	}
}

sub contacts : Chained('base'): Args(0) {
	my ( $self, $c ) = @_;

	my $following = q[];
	if ( $c->user_exists() ) {
		$following = $c->stash->{contacts_rs}->search(
			undef,
			{
				where => { userid => $c->user->id },
				rows  => 50,
				page  => 1,
			},
		);
		$c->stash( contacts => $following );
	}
	else {
		$c->stash->{'error_msg'} = 'You need to be logged in to do that';
		$c->res->redirect( $c->uri_for('/') );
	}
}

sub create : Chained('base'): Args(2) {
	my ( $self, $c, $userid, $goalid ) = @_;

	if ( $c->user_exists() ) {
		$c->stash( userid => $userid );
		$c->stash( goalid => $goalid );
		
		if ( lc($c->req->method) eq 'post' ) {
			$c->log->info('test');
			eval { $c->stash->{challages_rs}->create({
				challangeby => $c->user->id,
				challanged => $userid,
				goal => $goalid,
				challange => $c->req->params->{challange},
				});
			};
		}
	}
	else {
		$c->res->redirect( $c->uri_for('/') );
	}
}

=head1 AUTHOR

Adam Jimerson,,,4237900479,4233132702

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
