package eduity::Controller::Root;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config(namespace => '');

=head1 NAME

eduity::Controller::Root - Root Controller for eduity

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=head2 index

The root page (/)

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    # Hello World
	#$c->response->body( $c->welcome_message );
}

=head2 default

Standard 404 error page

=cut

sub default :Path {
    my ( $self, $c ) = @_;
    $c->response->body( 'Page not found' );
    $c->response->status(404);
}

=head2 end

Attempt to render a view, if needed.

=cut

sub end : ActionClass('RenderView') {}

sub login :Local :Args(0) {
	my ( $self, $c ) = @_;
	$c->stash->{'template'} = 'index.tt2';

	if ( exists($c->req->params->{'email'}) ) {
		my $data = {
			email => $c->req->params->{'email'},
			password => $c->req->params->{'password'},
		};
		if ( $c->authenticate( {
					email => $data->{email},
					password => $data->{password}
				}))
		{
			$c->log->info("valid login");
			$c->stash->{'status_msg'} = 'You have sucuessfully loged in';
			my $user = $c->user->get_object;
			if ( $user->active == 0 ) {
				$c->logout();
				$c->stash->{'error_msg'} = 'Your account has been suspended';
				return $c->res->redirect( $c->uri_for('/') );
			}
			eval { $user->update({
						lastlogin => 'now()',
					});
			};
		}
		else {
			$c->log->info("invalid login");
			$c->stash->{'error_msg'} = 'Invalid login!';
			return $c->res->redirect( $c->uri_for('/') );
		}
	}
		return $c->res->redirect( $c->uri_for('/profile/home') );
}

sub logout :Local :Args(0) {
	my ( $self, $c ) = @_;

	$c->stash->{'template'} = 'index.tt2';
	$c->logout();
	$c->stash->{'status_msg'} = 'You have been logged out.';
}

=head1 AUTHOR

Adam Jimerson,,,4237900479,4233132702

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
