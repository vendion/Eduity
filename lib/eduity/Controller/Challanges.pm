package eduity::Controller::Challanges;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

eduity::Controller::Challanges - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub base : Chained('/'): PathPart('challanges'):CaptureArgs(0) {
	my ( $self, $c ) = @_;

	$c->stash( users_rs => $c->model('DB::User') );
	$c->stash( challanges_rs => $c->model('DB::Challange') );
}

sub home : Chained('base'): Args(0) {
	my ( $self, $c ) = @_;

	if ( $c->user_exists() ) {
		my $challanges = $c->stash->{challanges_rs}->search(
			undef,
			{
				where => { 'challanged' => $c->user->id,
					'complete' => 'false'
				},
				rows => 50,
				page => 1,
			},
		);
		$c->stash( challanges => $challanges );
	}
	else {
		$c->stash->{'error_msg'} = 'You need to be logged in to view this page';
		$c->res->redirect( $c->uri_for('/') );
	}
}

sub done : Chained('base'): Args(0) {
	my ( $self, $c ) = @_;

	$c->stash->{'template'} = 'challanges/home.tt2';

	if ( $c->user_exists() ) {
		my $challanges = $c->stash->{challanges_rs}->search(
			undef,
			{
				where => { 'challanged' => $c->user->id,
					'complete' => 'true'
				},
				rows => 50,
				page => 1,
			},
		);
		$c->stash( challanges => $challanges );
	}
	else {
		$c->stash->{'error_msg'} = 'You need to be logged in to view this page';
		$c->res->redirect( $c->uri_for('/') );
	}
}

sub issued : Chained('base'): Args(0) {
	my ( $self, $c ) = @_;

	if ( $c->user_exists() ) {
		my $challanges = $c->stash->{challanges_rs}->search(
			undef,
			{
				where => { 'challangeby' => $c->user->id },
				rows => 50,
				page => 1,
			},
		);
		$c->stash( challanges => $challanges );
	}
	else {
		$c->res->redirect( $c->uri_for('/') );
	}
}
=head1 AUTHOR

Adam Jimerson,,,4237900479,4233132702

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
