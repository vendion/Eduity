use utf8;
package eduity::Schema::Futureskill;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

eduity::Schema::Futureskill

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::FS>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::FS");

=head1 TABLE: C<futureskills>

=cut

__PACKAGE__->table("futureskills");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'futureskills_id_seq'

=head2 userid

  data_type: 'integer'
  is_nullable: 0

=head2 skill

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "futureskills_id_seq",
  },
  "userid",
  { data_type => "integer", is_nullable => 0 },
  "skill",
  { data_type => "varchar", is_nullable => 0, size => 20 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07024 @ 2012-09-16 08:47:43
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:6aV1wHChRnQYf0a4zYNFOg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
