use utf8;
package eduity::Schema::User;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

eduity::Schema::User

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::FS>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::FS");

=head1 TABLE: C<users>

=cut

__PACKAGE__->table("users");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'users_id_seq'

=head2 username

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 password

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 firstname

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 lastname

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 email

  data_type: 'varchar'
  is_nullable: 0
  size: 20

=head2 active

  data_type: 'boolean'
  default_value: true
  is_nullable: 0

=head2 created

  data_type: 'timestamp with time zone'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}

=head2 lastlogin

  data_type: 'timestamp with time zone'
  default_value: '1999-01-01 00:00:01-05'
  is_nullable: 0

=head2 reputation

  data_type: 'smallint'
  default_value: 50
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "users_id_seq",
  },
  "username",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "password",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "firstname",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "lastname",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "email",
  { data_type => "varchar", is_nullable => 0, size => 20 },
  "active",
  { data_type => "boolean", default_value => \"true", is_nullable => 0 },
  "created",
  {
    data_type     => "timestamp with time zone",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
  },
  "lastlogin",
  {
    data_type     => "timestamp with time zone",
    default_value => "1999-01-01 00:00:01-05",
    is_nullable   => 0,
  },
  "reputation",
  { data_type => "smallint", default_value => 50, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<users_username_uniq>

=over 4

=item * L</username>

=back

=cut

__PACKAGE__->add_unique_constraint("users_username_uniq", ["username"]);

=head1 RELATIONS

=head2 profiles

Type: has_many

Related object: L<eduity::Schema::Profile>

=cut

__PACKAGE__->has_many(
  "profiles",
  "eduity::Schema::Profile",
  { "foreign.userid" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 userstoroles

Type: has_many

Related object: L<eduity::Schema::Userstorole>

=cut

__PACKAGE__->has_many(
  "userstoroles",
  "eduity::Schema::Userstorole",
  { "foreign.userid" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 roleids

Type: many_to_many

Composing rels: L</userstoroles> -> roleid

=cut

__PACKAGE__->many_to_many("roleids", "userstoroles", "roleid");


# Created by DBIx::Class::Schema::Loader v0.07024 @ 2012-09-16 10:07:07
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ic7KVJQSQxWdjud9PkaQbw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
