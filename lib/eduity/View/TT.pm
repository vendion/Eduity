package eduity::View::TT;

use strict;
use warnings;

use base 'Catalyst::View::TT';

__PACKAGE__->config(
	TEMPLATE_EXTENSION => '.tt2',
	render_die => 1,
	INCLUDE_PATH => [
		eduity->path_to( 'root' ),
	],
	WRAPPER => 'wrapper.tt2',
	expose_methods => [qw/format_datetime format_date format_bool display_rating get_userimg follow_user fetch_user_info/],
);

sub format_datetime {
	my ( $self, $c, $datetime ) = @_;

	if ( defined $datetime ) {
		$datetime =~ s/T/ /g;
		return $datetime;
	}
	else {
		return;
	}
}

sub format_date {
	my ( $self, $c, $date ) = @_;

	if ( defined $date ) {
		$date =~ s/T.*//g;
		return $date;
	}
	else {
		return;
	}
}

sub format_bool {
	my ( $self, $c, $bool ) = @_;

	if ( defined $bool ) {
		if ( $bool eq 'false' ) {
			$bool = 'No';
		}
		else {
			$bool = 'Yes';
		}
		return $bool;
	}
	else {
		return;
	}
}

sub display_rating {
	my ( $self, $c, $rating ) = @_;

	my $icons = q[];
	if ( defined $rating ) {
		for (my $i = 0; $i < $rating; $i++) {
			$icons .= '<i class="icon-star"></i>&nbsp;';
		}
		return $icons;
	}
	else {
		return;
	}
}

sub get_userimg {
	my ( $self, $c, $email ) = @_;

	use Gravatar::URL;
	my %opts = (
		rating  => 'pg',
		size    => 90,
		default => 'identicon'
	);

	my $url = q[];
	if ( ! $email ) {
		$url = gravatar_url( email => $c->user->email, %opts );
	}
	else {
		$url = gravatar_url( email => $email, %opts );
	}
	$c->log->info( 'Gravatar URL returned: ' . $url);
	return $url;
}

sub follow_user {
	my ( $self, $c, $id ) = @_;

	my $following = $c->model('DB::Contact')->find( { userid => $c->user->id, contactid => $id });
	my $link = q[];
	if (! $following ) {
		$link = "<a class='btn btn-small' href=" . $c->uri_for("/profile/follow/$id") . "><i class='icon-retweet'></i> Follow</a>";
	}
	else {
		$link = "<a class='btn btn-small' href=" . $c->uri_for("/profile/unfollow/$id") . "><i class='icon-ban-circle'></i> Unfollow</a>";
	}
	return $link;
}

sub fetch_user_info {
	my ( $self, $c, $contactid ) = @_;

	my $user = $c->model('DB::User')->find( { id => $contactid } );
	die "No user returned\n" if (! $user);
#	return $user->firstname;
	my $id = $user->id;
	my $line = "<tr><td>" . $user->firstname . "</td><td>" . $user->lastname . "</td><td>" . $user->email . "</td><td><a class='btn btn-small' href=" . $c->uri_for("/profile/view/$id") . ">View</a></td></tr>";
	return $line;
}
