package eduity;
use Moose;
use namespace::autoclean;

use Catalyst::Runtime 5.80;

# Set flags and add plugins for the application.
#
# Note that ORDERING IS IMPORTANT here as plugins are initialized in order,
# therefore you almost certainly want to keep ConfigLoader at the head of the
# list if you're using it.
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a Config::General file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root
#                 directory

use Catalyst qw/
    ConfigLoader
    Static::Simple
	Authentication
	Authorization::Roles
	Session
	Session::Store::File
	Session::State::Cookie
	AutoCRUD
/;

use DateTime;

extends 'Catalyst';

our $VERSION = '0.02';

# Configure the application.
#
# Note that settings in eduity.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

__PACKAGE__->config(
    name => 'eduity',
    # Disable deprecated behavior needed by old applications
    disable_component_resolution_regex_fallback => 1,
    enable_catalyst_header => 1, # Send X-Catalyst header
	'Plugin::Authentication' => {
		default => {
			credential => {
				class => 'Password',
				password_type => 'hashed',
				password_hash_type => 'SHA1'
			},
			store => {
				class => 'DBIx::Class',
				user_model => 'DB::User',
				role_relation => 'roles',
				role_field => 'role',
				use_userdada_from_session => '1'
			}
		}
	}
);

# Start the application
__PACKAGE__->setup();

sub datetime {
	my $tz = DateTime::TimeZone->new( name => 'local' );
	my $dt = DateTime->now( time_zone => $tz );
	return $dt;
}

=head1 NAME

eduity - Catalyst based application

=head1 SYNOPSIS

    script/eduity_server.pl

=head1 DESCRIPTION

[enter your description here]

=head1 SEE ALSO

L<eduity::Controller::Root>, L<Catalyst>

=head1 AUTHOR

Adam Jimerson,,,4237900479,4233132702

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
