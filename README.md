# Eduity #

Edutiy is a social talent/skill mapper and growth tracker, it supports finding and following other users as well as enabling users to create challenges for each other to help the achive future skills and goals.  This version was developed as a Proof-of-Concept during a 48 hour launch, all code is available under the MIT License (see the LICENSE file for details)

## Requirements ##

  * Perl 5.12+ installed and running
      It would work with 5.10 but will require alot of work
  * PostgreSQL 9.1 installed and running
      It was tested with 9.1 but should work with 8.4+

## Installing ##

## Configuration ##

## Running ##

## Bugs ##
