use strict;
use warnings;
use Test::More;


use Catalyst::Test 'eduity';
use eduity::Controller::Challanges;

ok( request('/challanges')->is_success, 'Request should succeed' );
done_testing();
