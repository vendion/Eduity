#!/usr/bin/perl 
    use FindBin;
    use DBIx::Class::Schema::Loader qw| make_schema_at |;
    make_schema_at(
       "eduity::Schema",
          {
   	           debug          => 1,
    	       use_namespaces => 0,
          		components     => [qw/ InflateColumn::DateTime InflateColumn::FS/],
       		    dump_directory => "$FindBin::Bin/../lib"
          },
		  [ "dbi:Pg:dbname=eduity", "", "" ]
	);
