#!/bin/bash

script/eduity_create.pl model DB DBIC::Schema eduity::Schema \
	        create=static components=TimeStamp,EncodedColumn \
		        'dbi:Pg:dbname=eduity' '' '' '{ AutoCommit => 1 }'

exit 0;
